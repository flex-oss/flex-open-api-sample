package vn.flex.sample.openapi.common.utils.datetime;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeUtil {

    private static final String TIME_ZONE = "Asia/Ho_Chi_Minh";

    public static long convertToEpochMilli(LocalDateTime dateTime) {
        return convertToZonedDateTime(dateTime).toInstant().toEpochMilli();
    }

    public static ZonedDateTime convertToZonedDateTime(LocalDateTime dateTime) {
        ZoneId zoneId = ZoneId.of(TIME_ZONE);
        return dateTime.atZone(zoneId);
    }
}
