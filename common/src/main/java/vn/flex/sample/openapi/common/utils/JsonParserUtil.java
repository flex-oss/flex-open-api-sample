package vn.flex.sample.openapi.common.utils;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;

public class JsonParserUtil {

    private final static ObjectMapper jsonMapper = new ObjectMapper(); // can use DI to inject

    static {
        jsonMapper
                // camelCase => snake_case
                .setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE)
                .setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)
                .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .registerModule(new JavaTimeModule()); // for java.time.LocalDateTime
    }

    public static ObjectMapper getJsonMapper() {
        return jsonMapper;
    }

    /**
     * Convert to json string
     * This should convert fields' order follow the properties of object (do not config sort for this jsonmapper)
     *
     * @param object
     * @return json string of object
     */
    public static String toJson(Object object) {
        try {
            return jsonMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            // log error here;
            throw new RuntimeException("Error '" + e.getMessage() + "' when parsing Json with object: " + object, e);
        }
    }

    public static <T> T fromJson(String object, Class<T> type) {
        try {
            return jsonMapper.readValue(object, type);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T deserializeFromByte(byte[] bytes, Class<T> type) {
        try {
            return jsonMapper.readValue(bytes, type);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
