package vn.flex.sample.openapi.infrastructure.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;
import vn.flex.sample.openapi.common.utils.JsonParserUtil;

@Slf4j
public class AbstractHttpServerPortAdapter {
    protected WebClient webClient;

    protected Mono<Exception> toException(ClientResponse response, String endpoint) {
        return toException(response, endpoint, null);
    }

    protected Mono<Exception> toException(ClientResponse response, String endpoint, Object requestBody) {
        return response
                .createException()
                .map(e -> {
                    logError(e, endpoint, requestBody == null ? "" : JsonParserUtil.toJson(requestBody));
                    return e;
                });
    }

    protected void logError(WebClientResponseException response, String endpoint, String requestBody) {
        log.error(
                "{}: error, uri: {},  request: {}, error: {}",
                this.getClass().getSimpleName(),
                endpoint,
                requestBody,
                response.getResponseBodyAsString()
        );
    }
}
