package vn.flex.sample.openapi.infrastructure.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties("flex.sample.openapi.partner")
public class FlexPartnerConfig {
    private String merchantId = "";
    private String partnerId = "";
    private String accessKey = "";
    private String secretKey = "";
}
