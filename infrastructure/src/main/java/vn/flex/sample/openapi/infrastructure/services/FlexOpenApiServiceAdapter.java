package vn.flex.sample.openapi.infrastructure.services;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;
import vn.flex.sample.openapi.common.utils.JsonParserUtil;
import vn.flex.sample.openapi.core.domain.port.flex.FlexOpenApiServicePort;
import vn.flex.sample.openapi.core.domain.port.flex.dto.*;
import vn.flex.sample.openapi.infrastructure.config.FlexOpenApiRoutingConfig;
import vn.flex.sample.openapi.infrastructure.config.FlexPartnerConfig;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;


@Slf4j
@Component
public class FlexOpenApiServiceAdapter implements FlexOpenApiServicePort {

    private final FlexOpenApiAdapter flexOpenApiAdapter;
    private final FlexPartnerConfig flexPartnerConfig;
    private final FlexOpenApiRoutingConfig flexOpenApiRoutingConfig;

    public FlexOpenApiServiceAdapter(FlexOpenApiAdapter flexOpenApiAdapter,
                                     FlexPartnerConfig flexPartnerConfig,
                                     FlexOpenApiRoutingConfig flexOpenApiRoutingConfig) {
        this.flexOpenApiAdapter = flexOpenApiAdapter;
        this.flexPartnerConfig = flexPartnerConfig;
        this.flexOpenApiRoutingConfig = flexOpenApiRoutingConfig;
    }

    @Override
    public CompletionStage<FlexBankVerifyResultInfo> bankVerify(FlexBankVerifyRequestInfo requestInfo) {
        var headers = buildRequestHeader(requestInfo);
        return flexOpenApiAdapter.executePost(
                flexOpenApiRoutingConfig.getBankVerifyUrl(),
                headers,
                requestInfo,
                FlexBankVerifyResultInfo.class);
    }

    @Override
    public CompletionStage<FlexMerchantBalancesResultInfo> merchantBalances(FlexMerchantBalancesRequestInfo requestInfo) {
        var headers = buildRequestHeader(requestInfo);
        return flexOpenApiAdapter.executePost(
                flexOpenApiRoutingConfig.getMerchantBalancesUrl(),
                headers,
                requestInfo,
                FlexMerchantBalancesResultInfo.class);
    }

    @Override
    public CompletionStage<FlexCreateDisbursementResultInfo> createDisbursement(FlexCreateDisbursementRequestInfo requestInfo) {
        var headers = buildRequestHeader(requestInfo);
        return flexOpenApiAdapter.executePost(
                flexOpenApiRoutingConfig.getCreateDisbursementUrl(),
                headers,
                requestInfo,
                FlexCreateDisbursementResultInfo.class);
    }

    @Override
    public CompletionStage<FlexDisbursementStatusResultInfo> getDisbursementStatus(FlexDisbursementStatusRequestInfo requestInfo) {
        var headers = buildRequestHeader(requestInfo);
        return flexOpenApiAdapter.executePost(
                flexOpenApiRoutingConfig.getDisbursementStatusUrl(),
                headers,
                requestInfo,
                FlexDisbursementStatusResultInfo.class);
    }

    @Override
    public CompletionStage<FlexDisbursementReconcileResultInfo> getDisbursementReconcile(FlexDisbursementReconcileRequestInfo requestInfo) {
        var headers = buildRequestHeader(requestInfo);
        return flexOpenApiAdapter.executePost(
                flexOpenApiRoutingConfig.getDisbursementReconcileUrl(),
                headers,
                requestInfo,
                FlexDisbursementReconcileResultInfo.class);
    }

    @Override
    public CompletionStage<FlexDisbursementApprovalResultInfo> approvalDisbursement(FlexDisbursementApprovalRequestInfo requestInfo) {
        var headers = buildRequestHeader(requestInfo);
        return flexOpenApiAdapter.executePost(
                flexOpenApiRoutingConfig.getDisbursementApprovalUrl(),
                headers,
                requestInfo,
                FlexDisbursementApprovalResultInfo.class);
    }


    private Map<String, List<String>> buildRequestHeader(Object bodyObject) {
        try {
            var signature = generateSignature(bodyObject, flexPartnerConfig.getSecretKey());
            return Map.of(
                    "X-PARTNER-ID", List.of(flexPartnerConfig.getPartnerId()),
                    "X-ACCESS-KEY", List.of(flexPartnerConfig.getAccessKey()),
                    "X-SIGNATURE", List.of(signature));
        } catch (Exception ex) {
            log.error("FlexOpenApiServiceAdapter > buildRequestHeader error: {}", ex.getMessage(), ex.getCause());
        }
        return Collections.emptyMap();
    }

    private String generateSignature(Object bodyObject, String secretKey) throws Exception {
        // Body to Json String
        String dataJson = JsonParserUtil.toJson(bodyObject);

        // Generate md5 String
        byte[] utfEncodeData = encodeUTF8(dataJson);
        String md5HashedData = md5Hash(utfEncodeData);

        // HMAC SHA256
        byte[] secretKeyBytes = secretKey.getBytes(StandardCharsets.UTF_8);
        Mac mac = Mac.getInstance("HmacSHA256");
        SecretKeySpec secretKeySpec = new SecretKeySpec(secretKeyBytes, "HmacSHA256");
        mac.init(secretKeySpec);

        return bytesToHex(mac.doFinal(md5HashedData.getBytes(StandardCharsets.UTF_8)));
    }

    private String md5Hash(byte[] data) {
        return DigestUtils.md5Hex(data);
    }

    private byte[] encodeUTF8(String payload) {
        return payload.getBytes(StandardCharsets.UTF_8);
    }

    private String bytesToHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }
}
