package vn.flex.sample.openapi.infrastructure.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import vn.flex.sample.openapi.common.utils.JsonParserUtil;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;
import java.util.function.Consumer;

@Slf4j
@Component
public class FlexOpenApiAdapter extends AbstractHttpServerPortAdapter {

    public FlexOpenApiAdapter(WebClient webClient) {
        this.webClient = webClient;
    }

    public <T> CompletionStage<T> executePost(String endPoint, Map<String, List<String>> headers, Object bodyData, Class<T> type) {
        MultiValueMap<String, String> mapHeaders = CollectionUtils.toMultiValueMap(headers);
        Consumer<HttpHeaders> headersConsumer = httpHeaders -> httpHeaders.addAll(mapHeaders);
        return webClient
                .post()
                .uri(endPoint)
                .accept(MediaType.APPLICATION_JSON)
                .headers(headersConsumer)
                .body(BodyInserters.fromValue(bodyData))
                .retrieve()
                .onStatus(HttpStatus::isError, response -> toException(response, endPoint, bodyData))
                .bodyToMono(type)
                .doOnNext(response -> log.info("FlexOpenApiAdapter: executePost, uri {} headers {} request {} result {}", endPoint, JsonParserUtil.toJson(headers), JsonParserUtil.toJson(bodyData), JsonParserUtil.toJson(response)))
                .toFuture();
    }
}
