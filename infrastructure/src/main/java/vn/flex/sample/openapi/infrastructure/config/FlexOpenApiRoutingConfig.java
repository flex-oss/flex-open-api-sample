package vn.flex.sample.openapi.infrastructure.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties("flex.sample.openapi.routing")
public class FlexOpenApiRoutingConfig {

    private String baseUrl = "https://api-sandbox.flexmoney.vn";

    private String bankVerify = "/partner/v1/bank/verify";
    private String merchantBalances = "/partner/v1/balances";
    private String createDisbursement = "/partner/v1/disbursement";
    private String disbursementStatus = "/partner/v1/disbursement/status";
    private String disbursementReconcile = "/partner/v1/disbursement/reconcile";
    private String disbursementApproval = "/partner/v1/disbursement/approval";

    public String getBankVerifyUrl() {
        return baseUrl + bankVerify;
    }

    public String getMerchantBalancesUrl() {
        return baseUrl + merchantBalances;
    }

    public String getCreateDisbursementUrl() {
        return baseUrl + createDisbursement;
    }

    public String getDisbursementStatusUrl() {
        return baseUrl + disbursementStatus;
    }

    public String getDisbursementReconcileUrl() {
        return baseUrl + disbursementReconcile;
    }

    public String getDisbursementApprovalUrl() {
        return baseUrl + disbursementApproval;
    }
}
