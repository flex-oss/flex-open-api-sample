# flex-openapi-sample

**Project Sample** for Flex Open Api 

**Document**

**Framework**
 - Spring webflux
 - Java 11
 - Maven

**How to build**
 - mvn clean install -U

**How to use**
 - Setup ENV: FLEX_OPEN_API_URL,FLEX_MERCHANT_ID,FLEX_PARTNER_ID,FLEX_ACCESS_KEY,FLEX_SECRET_KEY
 - Call http://localhost:8080/v1/sample/bank_verify for Bank Verify
 - Call http://localhost:8080/v1/sample/merchant_balances for Get Merchant Balances
 - Call http://localhost:8080/v1/sample/create_disbursement for Create Disbursement
 - Call http://localhost:8080/v1/sample/disbursement_status for Get Disbursement Status
 - Call http://localhost:8080/v1/sample/disbursement_reconcile for Get Disbursement Reconcile
 - Call http://localhost:8080/v1/sample/disbursement_approve for Approve Disbursement

