package vn.flex.sample.openapi.app.spring.context;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import vn.flex.sample.openapi.core.application.service.HomeApplicationService;
import vn.flex.sample.openapi.core.application.service.impl.HomeApplicationServiceImpl;
import vn.flex.sample.openapi.core.domain.port.flex.FlexOpenApiServicePort;

@Configuration
public class OpenApiAppCoreContext {

    private final FlexOpenApiServicePort flexOpenApiServicePort;

    public OpenApiAppCoreContext(FlexOpenApiServicePort flexOpenApiServicePort) {
        this.flexOpenApiServicePort = flexOpenApiServicePort;
    }


    @Bean
    public HomeApplicationService homeApplicationService() {
        return new HomeApplicationServiceImpl(flexOpenApiServicePort);
    }
}
