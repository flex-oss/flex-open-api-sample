package vn.flex.sample.openapi.app.spring.configuration.objectmapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import vn.flex.sample.openapi.common.utils.JsonParserUtil;

@Configuration
public class ObjectMapperConfiguration {
    @Bean
    public ObjectMapper objectMapper() {
        return JsonParserUtil.getJsonMapper();
    }
}
