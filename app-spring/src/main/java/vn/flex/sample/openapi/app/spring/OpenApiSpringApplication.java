package vn.flex.sample.openapi.app.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenApiSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpenApiSpringApplication.class, args);
	}
}
