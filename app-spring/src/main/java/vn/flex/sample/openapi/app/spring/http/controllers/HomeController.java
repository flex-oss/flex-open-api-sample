package vn.flex.sample.openapi.app.spring.http.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import vn.flex.sample.openapi.core.application.service.HomeApplicationService;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping
public class HomeController {

    private final HomeApplicationService homeApplicationService;

    public HomeController(HomeApplicationService homeApplicationService) {
        this.homeApplicationService = homeApplicationService;
    }

    @GetMapping
    public Map<String, String> index() {
        var response = new HashMap<String, String>();
        response.put("Service", "Flex OpenApi Sample !!!");

        return response;
    }

    @GetMapping(value = "/healthcheck")
    public Map<String, String> healthcheck() {
        var response = new HashMap<String, String>();
        response.put("result", "OK");

        return response;
    }

    @GetMapping(value = "/v1/sample/bank_verify")
    public Mono<String> sampleBankVerify() {
        var completionStage = homeApplicationService.sampleBankVerify();
        return Mono.fromCompletionStage(completionStage);
    }

    @GetMapping(value = "/v1/sample/merchant_balances")
    public Mono<String> sampleMerchantBalances() {
        var completionStage = homeApplicationService.sampleMerchantBalances();
        return Mono.fromCompletionStage(completionStage);
    }

    @GetMapping(value = "/v1/sample/create_disbursement")
    public Mono<String> sampleCreateDisbursement() {
        var completionStage = homeApplicationService.sampleCreateDisbursement();
        return Mono.fromCompletionStage(completionStage);
    }

    @GetMapping(value = "/v1/sample/disbursement_status")
    public Mono<String> sampleDisbursementStatus() {
        var completionStage = homeApplicationService.sampleDisbursementStatus();
        return Mono.fromCompletionStage(completionStage);
    }

    @GetMapping(value = "/v1/sample/disbursement_reconcile")
    public Mono<String> sampleDisbursementReconcile() {
        var completionStage = homeApplicationService.sampleDisbursementReconcile();
        return Mono.fromCompletionStage(completionStage);
    }

    @GetMapping(value = "/v1/sample/disbursement_approve")
    public Mono<String> sampleDisbursementApprove() {
        var completionStage = homeApplicationService.sampleDisbursementApprove();
        return Mono.fromCompletionStage(completionStage);
    }
}
