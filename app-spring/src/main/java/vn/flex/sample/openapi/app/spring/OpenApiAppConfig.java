package vn.flex.sample.openapi.app.spring;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import vn.flex.sample.openapi.infrastructure.Infrastructure;

@Configuration
@ComponentScan(basePackageClasses = {
        Infrastructure.class
})
@EntityScan(basePackageClasses = {
        Infrastructure.class
})
public class OpenApiAppConfig {
}
