package vn.flex.sample.openapi.app.spring.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.channel.ChannelOption;
import io.netty.handler.logging.LogLevel;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;
import reactor.netty.transport.logging.AdvancedByteBufFormat;

import java.time.Duration;

@Configuration
@Slf4j
@Data
public class WebClientConfiguration {
    // Determines the timeout in milliseconds until a connection is established.
    private int connectTimeout = 3000;

    // Determines the timeout in milliseconds until a response returned.
    private int responseTimeout = 3000;

    private final ObjectMapper objectMapper;

    @Bean
    public HttpClient httpClient() {
        return HttpClient
                .create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, connectTimeout)
                .responseTimeout(Duration.ofMillis(responseTimeout))
                .compress(true);
    }

    @Bean
    public WebClient webClient(HttpClient client) {
        return WebClient
                .builder()
                .clientConnector(new ReactorClientHttpConnector(client
                        .wiretap("reactor.netty.http.client.HttpClient",
                                LogLevel.DEBUG,
                                AdvancedByteBufFormat.TEXTUAL) // setup http request/response log
                ))
                .exchangeStrategies(exchangeStrategies())
                .build();
    }

    /**
     * use our objectmapper for webclient
     * @return exchange with objectmapper
     */
    public ExchangeStrategies exchangeStrategies() {
        return ExchangeStrategies.builder()
                .codecs(config -> {
                    config.defaultCodecs().jackson2JsonDecoder(new Jackson2JsonDecoder(objectMapper));
                    config.defaultCodecs().jackson2JsonEncoder(new Jackson2JsonEncoder(objectMapper));
                })
                .build();
    }
}
