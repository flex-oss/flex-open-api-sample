package vn.flex.sample.openapi.core.domain.port.flex.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlexBankVerifyRequestInfo {

    private List<BankAccount> bankAccounts;

    private Long requestTime;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class BankAccount {
        private String bankId;
        private String accountNo;
    }
}
