package vn.flex.sample.openapi.core.domain.port.flex.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlexDisbursementApprovalResultInfo {

    private List<DisbursementApproval> data;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DisbursementApproval {
        private String id;
        private String status;
    }
}
