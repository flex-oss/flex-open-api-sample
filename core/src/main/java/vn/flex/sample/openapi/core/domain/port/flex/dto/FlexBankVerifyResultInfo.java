package vn.flex.sample.openapi.core.domain.port.flex.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlexBankVerifyResultInfo {

    private List<BankResultInfo> data;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class BankResultInfo {
        private String bankId;
        private String bankName;
        private String accountNo;
        private String accountName;
        private String resultCode;
        private String resultMessage;
    }
}
