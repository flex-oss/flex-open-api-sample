package vn.flex.sample.openapi.core.application.service;

import java.util.concurrent.CompletionStage;

public interface HomeApplicationService {

    CompletionStage<String> sampleBankVerify();
    CompletionStage<String> sampleMerchantBalances();
    CompletionStage<String> sampleCreateDisbursement();
    CompletionStage<String> sampleDisbursementStatus();
    CompletionStage<String> sampleDisbursementReconcile();
    CompletionStage<String> sampleDisbursementApprove();
}
