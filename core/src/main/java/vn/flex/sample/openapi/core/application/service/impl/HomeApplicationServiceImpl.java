package vn.flex.sample.openapi.core.application.service.impl;

import vn.flex.sample.openapi.common.utils.JsonParserUtil;
import vn.flex.sample.openapi.common.utils.datetime.DateTimeUtil;
import vn.flex.sample.openapi.core.application.service.HomeApplicationService;
import vn.flex.sample.openapi.core.domain.port.flex.FlexOpenApiServicePort;
import vn.flex.sample.openapi.core.domain.port.flex.dto.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletionStage;

public class HomeApplicationServiceImpl implements HomeApplicationService {

    private final FlexOpenApiServicePort flexOpenApiServicePort;

    public HomeApplicationServiceImpl(FlexOpenApiServicePort flexOpenApiServicePort) {
        this.flexOpenApiServicePort = flexOpenApiServicePort;
    }

    @Override
    public CompletionStage<String> sampleBankVerify() {
        var listBankAccounts = List.of(FlexBankVerifyRequestInfo.BankAccount.builder()
                .bankId("bank_id")
                .accountNo("bank_number")
                .build());
        var requestInfo = FlexBankVerifyRequestInfo.builder()
                .bankAccounts(listBankAccounts)
                .requestTime(DateTimeUtil.convertToEpochMilli(LocalDateTime.now()))
                .build();
        return flexOpenApiServicePort.bankVerify(requestInfo)
                .thenApply(JsonParserUtil::toJson);
    }

    @Override
    public CompletionStage<String> sampleMerchantBalances() {
        var requestInfo = FlexMerchantBalancesRequestInfo.builder()
                .requestTime(DateTimeUtil.convertToEpochMilli(LocalDateTime.now()))
                .build();
        return flexOpenApiServicePort.merchantBalances(requestInfo)
                .thenApply(JsonParserUtil::toJson);
    }

    @Override
    public CompletionStage<String> sampleCreateDisbursement() {

        var payee = FlexCreateDisbursementRequestInfo.PayeeInfo.builder()
                .bankId("bank_id")
                .bankName("bank_name")
                .bankAccountName("bank_account_number")
                .bankAccountNumber("bank_account_number")
                .build();

        var disbursementInfo = FlexCreateDisbursementRequestInfo.DisbursementInfo.builder()
                .refCode(UUID.randomUUID().toString())
                .amount(1000000L)
                .note("disbursement_note")
                .payee(payee)
                .build();

        var requestInfo = FlexCreateDisbursementRequestInfo.builder()
                .data(List.of(disbursementInfo))
                .requestTime(DateTimeUtil.convertToEpochMilli(LocalDateTime.now()))
                .build();
        return flexOpenApiServicePort.createDisbursement(requestInfo)
                .thenApply(JsonParserUtil::toJson);
    }

    @Override
    public CompletionStage<String> sampleDisbursementStatus() {
        var ids = List.of("id1", "id2");
        var requestInfo = FlexDisbursementStatusRequestInfo.builder()
                .ids(ids)
                .requestTime(DateTimeUtil.convertToEpochMilli(LocalDateTime.now()))
                .build();
        return flexOpenApiServicePort.getDisbursementStatus(requestInfo)
                .thenApply(JsonParserUtil::toJson);
    }

    @Override
    public CompletionStage<String> sampleDisbursementReconcile() {

        var requestInfo = FlexDisbursementReconcileRequestInfo.builder()
                .from(1663803087L)
                .to(1663833087L)
                .requestTime(DateTimeUtil.convertToEpochMilli(LocalDateTime.now()))
                .build();

        return flexOpenApiServicePort.getDisbursementReconcile(requestInfo)
                .thenApply(JsonParserUtil::toJson);
    }

    @Override
    public CompletionStage<String> sampleDisbursementApprove() {

        var disbursementApproval = FlexDisbursementApprovalRequestInfo.DisbursementApproval.builder()
                .id("id1")
                .action("approve")
                .note("note")
                .build();

        var requestInfo = FlexDisbursementApprovalRequestInfo.builder()
                .data(List.of(disbursementApproval))
                .requestTime(DateTimeUtil.convertToEpochMilli(LocalDateTime.now()))
                .build();

        return flexOpenApiServicePort.approvalDisbursement(requestInfo)
                .thenApply(JsonParserUtil::toJson);
    }
}
