package vn.flex.sample.openapi.core.domain.port.flex.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlexCreateDisbursementRequestInfo {

    private List<DisbursementInfo> data;
    private Long requestTime;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DisbursementInfo {
        private String refCode;
        private Long amount;
        private String note;
        private PayeeInfo payee;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PayeeInfo {
        private String bankId;
        private String bankName;
        private String bankAccountName;
        private String bankAccountNumber;
    }
}
