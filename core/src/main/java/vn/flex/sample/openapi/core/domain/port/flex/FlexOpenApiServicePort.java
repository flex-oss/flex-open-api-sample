package vn.flex.sample.openapi.core.domain.port.flex;

import vn.flex.sample.openapi.core.domain.port.flex.dto.*;

import java.util.concurrent.CompletionStage;

public interface FlexOpenApiServicePort {

    CompletionStage<FlexBankVerifyResultInfo> bankVerify(FlexBankVerifyRequestInfo requestInfo);
    CompletionStage<FlexMerchantBalancesResultInfo> merchantBalances(FlexMerchantBalancesRequestInfo requestInfo);
    CompletionStage<FlexCreateDisbursementResultInfo> createDisbursement(FlexCreateDisbursementRequestInfo requestInfo);
    CompletionStage<FlexDisbursementStatusResultInfo> getDisbursementStatus(FlexDisbursementStatusRequestInfo requestInfo);
    CompletionStage<FlexDisbursementReconcileResultInfo> getDisbursementReconcile(FlexDisbursementReconcileRequestInfo requestInfo);
    CompletionStage<FlexDisbursementApprovalResultInfo> approvalDisbursement(FlexDisbursementApprovalRequestInfo requestInfo);
}
