package vn.flex.sample.openapi.core.domain.port.flex.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlexDisbursementReconcileResultInfo {

    private Long paidDisbursementAmount;
    private Long completedDisbursementCount;
    private Long rejectedDisbursementCount;
    private Long totalItems;
    private List<DisbursementItem> disbursementItems;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DisbursementItem {
        private String id;
        private String refCode;
        private Long amount;
        private String status;
        private String createdAt;
        private Long processingFee;
    }
}
