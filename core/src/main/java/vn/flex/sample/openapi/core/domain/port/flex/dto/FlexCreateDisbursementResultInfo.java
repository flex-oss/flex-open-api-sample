package vn.flex.sample.openapi.core.domain.port.flex.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlexCreateDisbursementResultInfo {

    private List<DisbursementInfo> data;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DisbursementInfo {
        private String id;
        private String refCode;
        private String status;
        private String createdAt;
        private Long processingFee;
        private Long amount;
        private String note;
        private PayeeInfo payee;
        private String resultCode;
        private String resultMessage;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PayeeInfo {
        private String bankId;
        private String bankName;
        private String bankAccountName;
        private String bankAccountNumber;
    }
}
