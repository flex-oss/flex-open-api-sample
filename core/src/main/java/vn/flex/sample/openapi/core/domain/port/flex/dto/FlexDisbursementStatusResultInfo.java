package vn.flex.sample.openapi.core.domain.port.flex.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlexDisbursementStatusResultInfo {

    private List<DisbursementStatus> data;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DisbursementStatus {
        private String id;
        private String refCode;
        private String status;
    }
}
