package vn.flex.sample.openapi.core.domain.port.flex.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlexMerchantBalancesResultInfo {

    private MerchantBalance data;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MerchantBalance {
        private Long payoutBalance;
    }
}
